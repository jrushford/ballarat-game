module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
    '^.+\\.m?js$': 'babel-jest',
  },
  transformIgnorePatterns: ['/node_modules/(?!(strip-ansi|ansi-regex|string-width|wrap-ansi)/)'],
};
