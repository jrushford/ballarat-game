// tests/cardUtils.test.ts

import { getNewCardsForRound, Card } from './utils';

describe('getNewCardsForRound', () => {
  test('should generate correct cards for a given round number', () => {
    const round = 123;
    const cards = getNewCardsForRound(round);

    // Convert round number into digits
    const digits = round.toString().split('').map(Number);

    // Generate expected cards
    let expectedCards: Card[] = [];
    for (const digit of digits) {
      const deck = generateExpectedDeck(digit);
      expectedCards = expectedCards.concat(deck);
    }

    // Since the cards are shuffled, sort them by winValue for comparison
    const sortedCards = cards.sort((a, b) => a.winValue - b.winValue);
    const sortedExpectedCards = expectedCards.sort((a, b) => a.winValue - b.winValue);

    expect(sortedCards).toEqual(sortedExpectedCards);
  });
});

// Helper function to generate the expected deck
function generateExpectedDeck(deckLevel: number): Card[] {
  const deck: Card[] = Array(deckLevel).fill({ winValue: 0 });
  deck.push({ winValue: deckLevel + 1 });
  return deck;
}
