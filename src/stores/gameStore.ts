import { defineStore } from 'pinia';
import { getNewCardsForRound, shuffleDeck, getNewBetChips } from './utils';
import { flipCard, startNextRound } from './actions';

export function initialState() {
  const initialGold = 20;
  const initialRound = 1;

  const initialDeck = shuffleDeck(getNewCardsForRound(initialRound));
  const initialBetChips = getNewBetChips(initialRound, initialGold);
  const initialHighestScoreThisGame = initialGold;
  const initialPersonalHighestScore = Number(localStorage.getItem('personalHighestScore')) || 0;

  return {
    gameId: Math.floor(Math.random() * 1000000),
    deck: initialDeck,
    gold: initialGold,
    round: initialRound,
    betAmount: initialRound,
    betChips: initialBetChips,
    highestScoreThisGame: initialHighestScoreThisGame,
    personalHighestScore: initialPersonalHighestScore,
    showSettingsModal: false,
    showAnalyticsModal: false,
    showRoundIntroModal: true,
    showRoundResultsModal: false,
    previousMinBet: 1,
  };
}

export const useGameStore = defineStore('game', {
  state: () => initialState(),
  actions: {
    flipCard(indexOfDeck: number) {
      flipCard(this, indexOfDeck);
    },
    resetGame() {
      localStorage.removeItem('gameState');
      const newState = initialState();
      this.$patch({
        ...newState,
        personalHighestScore: this.personalHighestScore,
      });
      this.showRoundIntroModal = true;
    },
    startNextRound() {
      startNextRound(this);
    },
    toggleSettingsModal() {
      this.showSettingsModal = !this.showSettingsModal;
    },
    toggleRoundIntroModal() {
      this.showRoundIntroModal = !this.showRoundIntroModal;
    },
    updateBetAmount(newBetAmount: number) {
      this.betAmount = newBetAmount;
    },
    setBetAmount(amount: number) {
      this.betAmount = amount;
    },
    getNewBetChips(round: number) {
      getNewBetChips(round, this.gold);
    },
    saveToLocalStorage() {
      const gameState = {
        deck: this.deck,
        gold: this.gold,
        round: this.round,
        betAmount: this.betAmount,
        highestScoreThisGame: this.highestScoreThisGame,
        personalHighestScore: this.personalHighestScore,
        showSettingsModal: this.showSettingsModal,
        showAnalyticsModal: this.showAnalyticsModal,
        showRoundIntroModal: this.showRoundIntroModal,
      };
      localStorage.setItem('gameState', JSON.stringify(gameState));
    },
    loadFromLocalStorage() {
      const savedState = localStorage.getItem('gameState');
      if (savedState) {
        const gameState = JSON.parse(savedState);
        this.$patch(gameState);
      }
    },
  },
});
