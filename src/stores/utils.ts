export interface Card {
  id: number;
  winValue: number; // The value of the card that determines winnings if the card is flipped.
  isFlipped: boolean; // Indicates whether the card has been flipped.
}
export interface BetChip {
  amount: number;
  label: string;
}

export function generateCardDeck(
  deckLevel: number = 1,
  roundCardIdStartingNumber: number = 0,
): Card[] {
  const deck: Card[] = Array(deckLevel)
    .fill(null)
    .map((_, index) => ({
      id: roundCardIdStartingNumber++, // Assign unique ID to each card to avoid duplicates across rounds
      winValue: 0,
      isFlipped: false,
    }));
  deck.push({ id: roundCardIdStartingNumber++, winValue: deckLevel + 1, isFlipped: false }); // Assign unique ID to the extra card
  return deck;
}

export function shuffleDeck(deck: Card[]): Card[] {
  // Implement your preferred shuffling algorithm here (e.g., Fisher-Yates shuffle)
  return deck.sort(() => Math.random() - 0.5);
}

export function getNewCardsForRound(round: number): Card[] {
  let roundCardIdStartingNumber = 0;
  const roundString = round.toString();
  const roundDigits = roundString.split('').map(Number);
  let newCards: Card[] = [];

  for (const digit of roundDigits) {
    const deckLevel = digit; // Each digit represents the deck level
    const deck = generateCardDeck(deckLevel, roundCardIdStartingNumber);
    roundCardIdStartingNumber += deck.length;
    newCards = newCards.concat(deck);
  }
  // Optionally shuffle the final deck
  return shuffleDeck(newCards);
}

export function getNewBetChips(round: number, gold: number): BetChip[] {
  const minBet = Math.floor(round / 3) + 1;
  const bet25 = Math.floor(gold * 0.25);
  const bet33 = Math.floor(gold * 0.33);
  const bet50 = Math.floor(gold * 0.5);

  return [
    {
      amount: minBet,
      label: `${minBet} 💰\nMin Bet`,
    },
    { amount: bet25, label: `25% \n${bet25} 💰` },
    { amount: bet33, label: `33% \n${bet33} 💰` },
    { amount: bet50, label: `50% \n${bet50} 💰` },
    { amount: gold, label: `All In \n${gold} 💰` },
  ];
}
