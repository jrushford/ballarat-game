import { shuffleDeck, getNewCardsForRound, getNewBetChips } from './utils';
import { initialState } from './gameStore';

export function flipCard(state: any, cardId: number) {
  if (state.gold < state.betAmount) return;

  const card = state.deck.find((c: any) => c.id === cardId);
  if (!card) return;

  console.log(card);
  card.isFlipped = true;

  state.gold -= state.betAmount;
  state.gold += card.winValue * state.betAmount;

  state.betChips = getNewBetChips(state.round, state.gold);
  state.betAmount = state.betChips[0].amount;

  // Update highest score for this game
  if (state.gold > state.highestScoreThisGame) {
    state.highestScoreThisGame = state.gold;
  }

  // Update personal highest score
  if (state.gold > state.personalHighestScore) {
    state.personalHighestScore = state.gold;
    localStorage.setItem('personalHighestScore', String(state.personalHighestScore));
  }

  state.saveToLocalStorage(); // Use store instance to call this function

  // Check if the round is complete (all cards flipped or discarded)
  if (state.deck.every((card) => card.isFlipped)) {
    setTimeout(() => {
      state.startNextRound();
    }, 1000);
  }

  // Check if the player has lost all their gold
  if (state.gold <= 0) {
    alert('You have lost all your gold! Game Over.');
  }
}

export function startNextRound(state: any) {
  state.round++;
  const newDeck = shuffleDeck(getNewCardsForRound(state.round));
  state.deck = newDeck;
  state.betChips = getNewBetChips(state.round, state.gold); // Update bet chips for the new card
  state.betAmount = state.betChips[0].amount;
  state.showRoundIntroModal = true; // Show round intro modal at the start of the new round
  //save to storage incase of refresh
  state.saveToLocalStorage();
}
