export async function clickRandomButton(page, logMessage) {
  const buttons = await page.$$('button:visible');
  if (buttons.length === 0) {
    logMessage('No visible buttons found on the page.');
    return;
  }

  logMessage(`Found ${buttons.length} visible buttons on the page.`);
  for (let i = 0; i < buttons.length; i++) {
    const buttonText = await buttons[i].innerText();
    logMessage(`Button ${i + 1}: "${buttonText?.trim() || 'No text'}"`);
  }

  const randomIndex = Math.floor(Math.random() * buttons.length);
  const randomButton = buttons[randomIndex];
  const randomButtonText = await randomButton.innerText();
  logMessage(`Clicking on a random button: "${randomButtonText?.trim() || 'No text'}"`);
  await randomButton.click();
}
