import fs from 'fs';
import { logMessage } from './logger.js';
import { startDevelopmentServer } from './serverUtils.js';
import { installPlaywright, setupBrowser } from './playwrightUtils.js';
import { closeOverlay } from './pageUtils.js';
import { playGameStrategically } from './playGameStrategically.js';

(async () => {
  try {
    const { startDev, serverPort } = await startDevelopmentServer(logMessage);
    await installPlaywright(logMessage);
    const { browser, page } = await setupBrowser(logMessage, serverPort);

    try {
      await closeOverlay(page, logMessage);
    } catch (error) {
      logMessage('No overlay found or failed to close overlay. Continuing...');
    }

    // Function to take screenshots at different points in the game
    async function takeScreenshot(stepName) {
      const screenshotPath = `snapshots/${stepName}.png`;
      await page.screenshot({ path: screenshotPath });
      logMessage(`Screenshot taken: ${screenshotPath}`);
    }

    logMessage('Starting to play the game strategically...');
    await page.waitForTimeout(1000);
    await playGameStrategically(page, logMessage, takeScreenshot);

    logMessage('Closing the browser...');
    await browser.close();

    logMessage('Killing the development server...');
    startDev.kill();

    logMessage('Script completed successfully. No further action required.');
  } catch (error) {
    logMessage(`An error occurred: ${error}`);
  }
})();
