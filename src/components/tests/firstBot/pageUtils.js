export async function closeOverlay(page, logMessage) {
  logMessage('Closing any overlay present...');
  const overlays = await page.$$('div[class*="overlay"], div[class*="modal"], .fixed.inset-0');
  for (const overlay of overlays) {
    if (await overlay.isVisible()) {
      const closeButton = await overlay.$(
        'button:has-text("Close"), button:has-text("OK"), [aria-label="close"]',
      );
      if (closeButton) {
        await closeButton.click().catch(() => {});
      } else {
        await page.mouse.click(10, 10, { delay: 100 }).catch(() => {});
      }
    }
  }
}
