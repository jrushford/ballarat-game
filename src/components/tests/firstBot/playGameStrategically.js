export async function playGameStrategically(page, logMessage, takeScreenshot) {
  try {
    logMessage('Starting the game loop...');
    let loopCount = 0; // Optional: Keep track of loop iterations

    // **Clear Local Storage When the Game Loads**
    logMessage('Clearing local storage before starting the game.');
    await page.evaluate(() => localStorage.clear());

    // **Initialize a Map to Keep Track of Modal Counts**
    const modalCounts = new Map();

    while (true) {
      loopCount++;
      logMessage(`\n=== Loop iteration: ${loopCount} ===`);

      // First, check for any open modals
      logMessage('Checking for any open modals...');
      const modal = await page.$(
        'div[class*="modal"]:visible, div[class*="overlay"]:visible, .fixed.inset-0:visible',
      );

      if (modal) {
        // **Extract Modal Identifier**
        let modalIdentifier = 'modal';

        // Try to get a unique identifier for the modal
        const modalTitleElement = await modal.$('h1, h2, h3, .modal-title');
        if (modalTitleElement) {
          const modalTitle = await modalTitleElement.innerText();
          modalIdentifier = modalTitle.trim().replace(/\s+/g, '_');
        } else {
          // If no title is found, get some text content from the modal
          const modalText = await modal.innerText();
          modalIdentifier = modalText.trim().substring(0, 20).replace(/\s+/g, '_');
        }

        // **Update Modal Count**
        let modalCount = modalCounts.get(modalIdentifier) || 0;
        modalCounts.set(modalIdentifier, ++modalCount);

        logMessage(`Modal detected: "${modalIdentifier}". Taking snapshot before interacting.`);
        await takeScreenshot(`${modalIdentifier}`); // Screenshot filename is based on modalIdentifier

        // Find a button within the modal and click it
        const modalButton = await modal.$('button:visible');
        if (modalButton) {
          logMessage('Clicking button in the modal.');
          await modalButton.click();

          // Wait for one second after closing the modal
          await page.waitForTimeout(1000);

          // Start the loop again after closing the modal
          continue;
        } else {
          logMessage('No visible button found in the modal.');
          // If necessary, add additional logic to handle modals without buttons
          continue;
        }
      } else {
        logMessage('No modal detected. Proceeding with game logic.');
      }

      // Wait for any new elements (cards and betting options) to load
      await page.waitForTimeout(1000); // Wait for one second to allow elements to load

      // Find all card buttons and betting options buttons
      logMessage('Finding card buttons and betting options...');

      // **Card Buttons Selector**
      // Adjust this selector to match the actual classes or attributes of your card buttons
      const cardButtons = await page.$$('button:has(div.w-20.h-32.rounded-lg):visible');

      // **Betting Options Buttons Selector**
      // Adjust this selector to match the actual classes or attributes of your betting option buttons
      const bettingOptionButtons = await page.$$('button.betting-option:visible');

      // Combine all buttons
      const allButtons = [...cardButtons, ...bettingOptionButtons];

      if (allButtons.length > 0) {
        logMessage(`Found ${allButtons.length} interactive buttons on the page.`);

        // Click a random button
        const randomIndex = Math.floor(Math.random() * allButtons.length);
        const randomButton = allButtons[randomIndex];
        logMessage(`Clicking on a random button: Button ${randomIndex + 1}`);

        await randomButton.click();

        // Wait for a bit after clicking
        await page.waitForTimeout(1000);

        // Start the loop again
        continue;
      } else {
        logMessage('No interactive buttons found on the page.');
        // Wait for a bit before retrying
        await page.waitForTimeout(2000);
        continue;
      }
    }
  } catch (error) {
    logMessage(`An error occurred while playing the game: ${error.message}`);
    logMessage(`Error stack trace: ${error.stack}`);
    await takeScreenshot('error_state');
  } finally {
    // **Clear Local Storage When the Game Closes**
    logMessage('Clearing local storage before closing the game.');
    await page.evaluate(() => localStorage.clear());
  }
}
