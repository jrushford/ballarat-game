import { exec } from 'child_process';

export function startDevelopmentServer(logMessage) {
  logMessage('Starting game...');
  const startDev = exec('yarn dev');

  return new Promise((resolve) => {
    let serverPort = '5173'; // Default port
    startDev.stdout?.on('data', (data) => {
      const portMatch = data.match(/http:\/\/localhost:(\d+)/);
      if (portMatch) {
        serverPort = portMatch[1];
      }

      if (
        data.toLowerCase().includes('compiled successfully') ||
        data.toLowerCase().includes('ready')
      ) {
        logMessage('Server is ready.');
        resolve({ startDev, serverPort });
      }
    });

    startDev.stderr?.on('data', (data) => {
      logMessage(`Server error: ${data}`);
    });
  });
}
