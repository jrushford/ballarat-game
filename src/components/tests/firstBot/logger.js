import fs from 'fs';

export function logMessage(message) {
  const timestamp = new Date().toISOString();
  const formattedMessage = `[${timestamp}] ${message}`;
  console.log(formattedMessage);
  fs.appendFileSync('script_log.txt', formattedMessage + '\n');
}
