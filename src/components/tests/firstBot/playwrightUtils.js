import { chromium } from 'playwright';
import { promisify } from 'util';
import { exec } from 'child_process';

const execPromise = promisify(exec);

export async function installPlaywright(logMessage) {
  logMessage('Installing Playwright browsers...');
  await execPromise('yarn playwright install');
}

export async function setupBrowser(logMessage, serverPort) {
  logMessage('Launching browser...');
  const browser = await chromium.launch({ headless: false });
  const context = await browser.newContext();
  const page = await context.newPage();

  logMessage(`Navigating to http://localhost:${serverPort}/ballarat-game/`);
  await page.goto(`http://localhost:${serverPort}/ballarat-game/`, { waitUntil: 'networkidle' });

  return { browser, context, page };
}
