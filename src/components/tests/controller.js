import { spawn } from 'child_process';
import chokidar from 'chokidar';
import fs from 'fs';
import path from 'path';
import dotenv from 'dotenv';
import { GoogleGenerativeAI, HarmCategory, HarmBlockThreshold } from '@google/generative-ai';

dotenv.config();

let childProcess = null;
let timeoutHandle = null;
let retryCount = 0;
const maxRetries = 3;

const apiKey = process.env.GEMINI_API_KEY;
if (!apiKey) {
  console.error('Error: GEMINI_API_KEY environment variable is not set.');
  process.exit(1);
}

const genAI = new GoogleGenerativeAI(apiKey);

const model = genAI.getGenerativeModel({
  model: 'gemini-1.5-flash',
});

const generationConfig = {
  temperature: 1,
  topP: 0.95,
  topK: 64,
  maxOutputTokens: 8192,
  responseMimeType: 'application/json',
};

const logFilePath = 'script_log.txt';

function logMessage(message) {
  const timestamp = new Date().toISOString();
  const logEntry = `[${timestamp}] ${message}`;
  fs.appendFile(logFilePath, `${logEntry}\n`, 'utf-8', (err) => {
    if (err) {
      console.error('Error writing to log file:', err);
    }
  });
  console.log(logEntry); // Log to console as well
}

async function callGoogleGeminiAPI(errorMessage, scriptContent) {
  try {
    const prompt = `I encountered the following issue while running the script:
  
  Issue: ${errorMessage}
  
  Here is the content of the script:
  
  ${scriptContent}
  
  Please provide only the JavaScript code as a solution.`;

    const result = await model.generateText({
      prompt,
      ...generationConfig,
    });

    if (result?.candidates && Array.isArray(result.candidates) && result.candidates.length > 0) {
      const responseText = result.candidates[0]?.content;
      if (typeof responseText !== 'string') {
        throw new Error('Received unexpected response format from Google Gemini API.');
      }
      fs.writeFileSync('google_gemini_response.txt', responseText, 'utf-8');
      logMessage('Google Gemini API response saved to google_gemini_response.txt');
      retryCount = 0;
      updateTryingToWinScript(); // Update the script automatically after getting a response
    } else {
      throw new Error('No candidates received from Google Gemini API.');
    }
  } catch (error) {
    logMessage(`Error calling Google Gemini API: ${error.message}`);
    if (retryCount < maxRetries) {
      retryCount++;
      logMessage(`Retrying Google Gemini API call (${retryCount}/${maxRetries})...`);
      await callGoogleGeminiAPI(errorMessage, scriptContent);
    } else {
      logMessage('Max retries reached. Not retrying further.');
    }
  }
}

function startScript() {
  const scriptPath = 'src/components/tests/firstBot/main.js';
  const scriptContent = fs.readFileSync(scriptPath, 'utf-8');

  if (childProcess) {
    childProcess.kill('SIGTERM');
    logMessage('Previous process killed. Restarting...');
  }

  logMessage('Starting script...');
  childProcess = spawn('node', [scriptPath], {
    stdio: ['pipe', 'pipe', 'pipe'],
  });

  resetInactivityTimeout();

  childProcess.stdout.on('data', (data) => {
    const logEntry = `stdout: ${data}`;
    logMessage(logEntry);
    resetInactivityTimeout(); // Reset timeout on activity
  });

  childProcess.stderr.on('data', (data) => {
    const logEntry = `stderr: ${data}`;
    logMessage(logEntry);
    console.error(logEntry); // Show error in console as well
    callGoogleGeminiAPI(`Error from main.js: ${data}`, scriptContent);
  });

  childProcess.on('close', (code) => {
    clearTimeout(timeoutHandle);
    logMessage(`Child process closed with code ${code}`);
    if (code === 0) {
      logMessage('Child process exited successfully with code 0. No further action required.');
    } else {
      callGoogleGeminiAPI(`Child process exited with code ${code}`, scriptContent);
    }
  });

  childProcess.on('error', (error) => {
    clearTimeout(timeoutHandle);
    logMessage(`Child process error: ${error.message}`);
    console.error(`Child process error: ${error.message}`); // Show error in console as well
    callGoogleGeminiAPI(`Child process error: ${error.message}`, scriptContent);
  });
}

function resetInactivityTimeout() {
  clearTimeout(timeoutHandle);
  timeoutHandle = setTimeout(() => {
    logMessage(
      'Script inactivity timeout reached. Stopping script and sending prompt to Google Gemini API.',
    );
    if (childProcess) {
      childProcess.kill('SIGTERM');
      logMessage('Child process killed due to inactivity.');
    }
    callGoogleGeminiAPI(
      'Script was inactive for too long. Possible timeout or hanging process.',
      fs.readFileSync('src/components/tests/firstBot/main.js', 'utf-8'),
    );
  }, 5000); // 5-second timeout
}

async function updateTryingToWinScript() {
  try {
    const responseContent = fs.readFileSync('google_gemini_response.txt', 'utf-8');

    if (validateJavaScriptSyntax(responseContent)) {
      fs.writeFileSync('src/components/tests/firstBot/main.js', responseContent, 'utf-8');
      logMessage('New version of main.js has been saved. Restarting script...');
      startScript(); // Restart the script automatically after updating
    } else {
      logMessage('Validation error in response content. Not saving new version of main.js.');
    }
  } catch (error) {
    logMessage(`Error updating main.js: ${error.message}`);
  }
}

function validateJavaScriptSyntax(code) {
  try {
    new Function(code);
    return true;
  } catch (error) {
    return false;
  }
}

// Start the script initially
startScript();

// Watch for changes to restart the script
chokidar.watch('src/components/tests/firstBot/main.js').on('change', () => {
  logMessage('File updated. Restarting script...');
  startScript();
});

// Watch for changes to restart the script
chokidar.watch('src/components/tests/firstBot/playGameStrategically.js').on('change', () => {
  logMessage('File updated. Restarting script...');
  startScript();
});
