import { chromium } from 'playwright';
import { exec } from 'child_process';
import { promisify } from 'util';

const execPromise = promisify(exec);

(async () => {
  try {
    // Start the development server
    console.log('Starting game...');
    const startDev = exec('yarn dev');

    let serverPort = '5173'; // Default port

    // Wait for the development server to be ready
    await new Promise((resolve) => {
      startDev.stdout?.on('data', (data) => {
        console.log(data); // Log server output to help debug

        // Extract port from the console output
        const portMatch = data.match(/http:\/\/localhost:(\d+)/);
        if (portMatch) {
          serverPort = portMatch[1];
        }

        if (
          data.toLowerCase().includes('compiled successfully') ||
          data.toLowerCase().includes('ready')
        ) {
          console.log('Server is ready.');
          resolve();
        }
      });
      startDev.stderr?.on('data', (data) => {
        console.error(`Server error: ${data}`);
      });
    });

    // Install Playwright browsers using yarn
    console.log('Installing Playwright browsers...');
    await execPromise('yarn playwright install');

    // Launch a browser instance
    const browser = await chromium.launch({ headless: false });
    const context = await browser.newContext();
    const page = await context.newPage();

    // Navigate to the local development server using the detected port
    await page.goto(`http://localhost:${serverPort}/ballarat-game/`, { waitUntil: 'networkidle' });

    // Function to close overlay if it exists (faster approach)
    async function closeOverlay() {
      const overlays = await page.$$('div[class*="overlay"], div[class*="modal"], .fixed.inset-0');
      for (const overlay of overlays) {
        if (await overlay.isVisible()) {
          console.log('Closing overlay...');
          const closeButton = await overlay.$(
            'button:has-text("Close"), button:has-text("OK"), [aria-label="close"]',
          );
          if (closeButton) {
            await closeButton.click().catch(() => {
              console.log('No close button found or unable to click overlay button.');
            });
          } else {
            // Attempt to dismiss overlay by clicking outside
            await page.mouse.click(10, 10, { delay: 100 }).catch(() => {
              console.log('Unable to dismiss overlay by clicking outside.');
            });
          }
        }
      }
    }

    // Close any overlay before proceeding
    await closeOverlay();

    // Function to click all clickable elements on the page, with reduced frequency for certain buttons
    async function clickAllClickableElements() {
      let clickableElements = await page.$$('button, a, [role="button"], [onclick]');
      const clickedElements = new Set();

      while (clickableElements.length > 0) {
        for (const element of clickableElements) {
          try {
            const textContent = await element.textContent();
            const isSettingsButton = textContent && textContent.toLowerCase().includes('settings');
            const isAnalyticsButton =
              textContent && textContent.toLowerCase().includes('analytics');
            const shouldClickSettings = Math.random() < 0.01; // 1% chance to click the settings button
            const shouldClickAnalytics = Math.random() < 0.01; // 1% chance to click the analytics button

            if (
              (await element.isVisible()) &&
              (await element.isEnabled()) &&
              !clickedElements.has(element) &&
              (!isSettingsButton || shouldClickSettings) &&
              (!isAnalyticsButton || shouldClickAnalytics)
            ) {
              const box = await element.boundingBox();
              if (box) {
                // Check if the element is within the viewport
                await element.scrollIntoViewIfNeeded({ timeout: 500 });
                await element.click({ timeout: 1000 }); // Reduce click timeout to speed up retries
                clickedElements.add(element);
              }
            }
          } catch (e) {
            console.log(`Unable to click element: ${e.message}`);
            if (e.message.includes('intercepts pointer events')) {
              console.log('Skipping this element as it is obstructed.');
              continue;
            }
          }
        }
        // Update the list of clickable elements
        clickableElements = await page.$$('button, a, [role="button"], [onclick]');
      }
    }

    // Click all available buttons and links on the page
    await clickAllClickableElements();

    // Close the browser
    await browser.close();

    // Kill the development server
    startDev.kill();
  } catch (error) {
    console.error('An error occurred:', error);
  }
})();
